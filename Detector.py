from CameraController import XiaomiYiController
import cv2
import time
import mss
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from imutils.video import FileVideoStream
import pickle

face_csc = cv2.CascadeClassifier('/Users/teostock/PycharmProjects/DFDC/venv/lib/python3.7/site-packages/cv2/data/haarcascade_eye.xml')

def DetectByIPCamera(MonitorParameters, ShowDetection):
    
    Camera = XiaomiYiController()
    
    Camera.ConnectToServer()

    with mss.mss() as ScreenCapturing:
    
        ScreenObject = MonitorParameters #{"top": 40, "left": 0, "width": 550, "height": 330}
    
        cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    
        Camera.PartOneStartVideoToGetCapture()
    
        while "Screen capturing":
        
            img = np.array(ScreenCapturing.grab(ScreenObject))
        
            gray = cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY)
        
            eyes = face_csc.detectMultiScale(gray, scaleFactor = 1.3, minNeighbors = 4, minSize = (10, 10))
        
            for (x, y, w, h) in eyes:
                
                if ShowDetection == True:
                    
                    cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 0, 0), 2)
                
                    cv2.imshow("output", gray)

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        
                        break
                    

def DetectByWebCamera(ShowDetection):
    
    while True:
    
        cap = cv2.VideoCapture(0)

        ret, img = cap.read()

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        eyes = face_csc.detectMultiScale(gray, scaleFactor = 1.3, minNeighbors = 4 , minSize = (10, 10))

        for (x, y, w, h) in eyes:
    
            if ShowDetection == True:
            
                cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 0, 0), 2)
        
                cv2.imshow("output", gray)

                if cv2.waitKey(1) & 0xFF == ord('q'):

                    break
                    
def DetectByVideoFile(VideoFile):
    
    xa = []
    
    ya = []
    
    ha = []
    
    wa = []

    v_cap = FileVideoStream(VideoFile).start()
    
    v_len = int(v_cap.stream.get(cv2.CAP_PROP_FRAME_COUNT))

    for j in range(v_len):
    
        frame = v_cap.read()
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        eyes = face_csc.detectMultiScale(frame, scaleFactor = 1.3, minNeighbors = 4, minSize = (10, 10))

        for (x, y, w, h) in eyes:
        
            xa.append(x)
    
            ya.append(y)
    
            wa.append(w)
    
            ha.append(h)

            # cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
            #
            # cv2.imshow("output", frame)
            #
            # if cv2.waitKey(1) & 0xFF == ord('q'):
            #
            #     break
            # # v_cap.release()
            #
            #     cv2.destroyAllWindows()
            
    print('Preprocessed')
    
    Data = pd.DataFrame({'X': xa, 'Y': ya, 'Width': wa, 'Height': ha})

    Data = Data.groupby(np.arange(len(Data)) // 10).mean()

   Ypred = loaded_model.predict_proba(Data)[:,0] #not work
    
    plt.plot(Data.index, YPred)
    
    plt.show()
    
    
    


